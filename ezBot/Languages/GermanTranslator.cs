﻿namespace ezBot.Languages
{
    //Translated by Hesa from Elobuddy.net
    public class GermanTranslator : ITranslator
    {
        public string EzBot { get { return "ezBot - automatische Warteschlange für LoL: {0}"; } }
        public string By { get { return "Von Tryller kreiert und von Hesa geupdated und geändert."; } }
        public string Version { get { return "Version: {0}"; } }
        public string Support { get { return "Hilfe auf Discord: https://discord.gg/Fg2tQGa"; } }
        public string Garena { get { return "Garena wird jetzt auch unterstützt!"; } }
        public string SourceCode { get { return "Ich habe den Quelltext auf github unter folgendem Link hochgeladen: github.com/hesa2020/HesaElobuddy."; } }
        public string Issues { get { return "Bitte melde Fehler bei dem ezBot Forum Beitrag auf Elobuddy.net, der von Hesa veröffentlicht wurde."; } }
        public string AdministratorRequired { get { return "ezBot muss als Administrator ausgeführt werden."; } }
        public string ConfigLoaded { get { return "Konfiguration geladen."; } }
        public string LauncherPathInvalid { get { return "Dein LoL-Launcher Pfad ist falsch."; } }
        public string PleaseTryThis { get { return "Bitte versuche folgendes:"; } }
        public string LauncherFix1 { get { return "1. Stelle sicher, das der Pfad des LoL-Launchers ein ORDNER ist und keine .exe Datei."; } }
        public string LauncherFix2 { get { return "2. Der Pfad des Launchers sollte mit einem \\ enden."; } }
        public string LauncherFix3 { get { return "3. Gehe zum Launcher Pfad."; } }
        public string LauncherFix4 { get { return "4. Gehe zu RADS\\solutions\\lol_game_client_sln\\releases\\"; } }
        public string LauncherFix5 { get { return "5. Lösche alle Ordner außer: 0.0.1.154"; } }
        public string ChangingGameConfig { get { return "Ändere Spiel Konfiguration."; } }
        public string LoadingAccounts { get { return "Lade Accounts."; } }
        public string MaximumBotsRunning { get { return "Anzahl maximaler Bots: {0}"; } }
        public string YouMayHaveAnIssueInAccountsFile { get { return "Es könnte ein Fehler in accounts.txt bestehen."; } }
        public string AccountsStructure { get { return "Das Account Muster sieht so aus: ACCOUNT|PASSWORD|REGION|QUEUE_TYPE|IS_LEADER"; } }
        public string ErrorGetGarenaToken { get { return "Es gab einen Fehler beim Abrufen des Garena Tokens."; } }
        public string ErrorLeagueGameCfgRegular { get { return "Regulärer LoL game.cfg Fehler: Wenn du ein VMWare Ordner benutzt wird, dann stelle sicher, dass er nicht auf schreibgeschützt gestellt ist.\nException: {0}"; } }
        public string ErrorLeagueGameCfgGarena { get { return "Garena LoL game.cfg Fehler: Wenn du ein VMWare Ordner benutzt wird, dann stelle sicher, dass er nicht auf schreibgeschützt gestellt ist.\nException: {0}"; } }
        public string NoMoreAccountsToLogin { get { return "Keine Accounts mehr zum einloggen."; } }
        public string GameModeInvalid { get { return "Spielmodus nicht zulässig. Bitte wähle ein verfügbaren Modi."; } }
        public string WillShutdownOnceCurrentMatchEnds { get { return "Der Computer wird herruntergefahren, sobald das aktuelle Spiel beendet wurde."; } }
        public string EzBotGameStatus { get { return "ezBot - {0} Insgesammt - {1} Gewonnen - {2} Verloren"; } }
        public string AcceptingLobbyInvite { get { return "{0}: Akzeptiere Spieleinladung."; } }
        public string AllPlayersAccepted { get { return "Alle Spieler haben angenommen. Starte Warteschlange."; } }
        public string PlayersAcceptedCount { get { return "{0}/{1} Spieler haben angenommen, warte darauf bis alle akzeptiert haben."; } }
        public string EnteringChampionSelect { get { return "{0}: Betrete Championselect."; } }
        public string YouAreInChampionSelect { get { return "{0}: Du bist in der Championauswahl."; } }
        public string SelectedChampion { get { return "{1}: Wähle Champion: {0}."; } }
        public string WaitingForOtherPlayersLockin { get { return "{0}: Warte bis andere Spieler sich eingeloggt haben. "; } }
        public string ChampionNotAvailable { get { return "{1}: Champion '{0}' nicht in Besitz, ist nicht kostenlos spielbar oder wurde schon gewählt."; } }
        public string WaitingChampSelectTimer { get { return "{0}: Warte bis der Championselect Timer 0 erreicht hat."; } }
        public string YouAreInQueue { get { return "{0}: In Warteschlange."; } }
        public string ReQueued { get { return "{1}: Warteschlange neu gestartet: {0}."; } }
        public string QueuePopped { get { return "{0}: Match gefunden."; } }
        public string AcceptedQueue { get { return "{0}: Match annehmen!"; } }
        public string YouHaveLeaverBuster { get { return "Du hast einen Leaverbuster."; } }
        public string LaunchingLeagueOfLegends { get { return "{0}: Starte League of Legends."; } }
        public string ClosingGameClient { get { return "{0}: Schließe Spiel Client."; } }
        public string InQueueAs { get { return "{1}: In Warteschlange: {0}."; } }
        public string QueueFailedReason { get { return "Warteschlange fehlgeschlagen, Grund: {0}."; } }
        public string LeaverBusterTaintedWarningError { get { return "Leaver buster Tainted Warning error:\n{0}"; } }
        public string WaitingDodgeTimer { get { return "Warte auf Dodge Timer: {0} minutes!"; } }
        public string WaitingLeaverTimer { get { return "Warte auf Leaverbuster Timer: {0} minutes!"; } }
        public string JoinedLowPriorityQueue { get { return "Betrete Warteschlange mit niedriger Priorität! als {0}."; } }
        public string ErrorJoiningLowPriorityQueue { get { return "Es gab einen Fehler beim betreten der Warteschlange niedriger Priorität.\nDisconnecting."; } }
        public string ErrorOccured { get { return "Es ist ein Fehler aufgetreten:\n{0}"; } }
        public string RestartingLeagueOfLegends { get { return "{0}: Starte League of Legends neu."; } }
        public string RestartingLeagueOfLegendsAt { get { return "{1}: Starte League of Legends neu in {0} bitte warten."; } }
        public string PositionInLoginQueue { get { return "Position in der Einloggwarteschlange: {0}."; } }
        public string LoggingIntoAccount { get { return "Logge ein..."; } }
        public string SummonerDoesntExist { get { return "Beschwörer wurde nicht auf dem Account gefunden."; } }
        public string CreatingSummoner { get { return "Erstelle Beschwörer..."; } }
        public string CreatedSummoner { get { return "Beschwörer erstellt: {0}."; } }
        public string AlreadyMaxLevel { get { return "Beschwörer: {0} hat bereits das maximale Level erreicht."; } }
        public string LogIntoNewAccount { get { return "Logge in neuen Account ein."; } }
        public string BuyingXpBoost { get { return "Kaufe XP Boost."; } }
        public string CouldntBuyBoost { get { return "Konnte Boost nicht kaufen:\n{0}"; } }
        public string Normal5Requirements { get { return "Du musst Level 3 sein um NORMAL_5X5 queue spielen zu können."; } }
        public string JoinCoopBeginnerUntil { get { return "Betrete Kooperatives Spiel (Anfänger) Warteschlange {0}."; } }
        public string NeedLevel6BeforeAram { get { return "Du musst Level 6 sein um ARAM spielen zu können."; } }
        public string NeedLevel7Before3v3 { get { return "Du musst Level 3 sein um  NORMAL_3X3 spielen zu können."; } }
        public string Welcome { get { return "Willkommen {0} - Level ({1}) EP: ({2}) - XP: ({3} / {4})."; } }
        public string SendingGameInvites { get { return "Sende Spieleinladungen."; } }
        public string WaitingGameInviteFrom { get { return "Warte auf Spieleinladung von: {0}."; } }
        public string LevelUp { get { return "{1}: Level aufgestiegen: {0}."; } }
        public string CurrentRp { get { return "Deine aktuellen RP: {0}."; } }
        public string CurrentIp { get { return "Deine aktuellen EP: {0}."; } }
        public string CharacterReachedMaxLevel { get { return "Dein Beschwörer hat das maximale Level erreicht: {0}."; } }
        public string DownloadingMasteries { get { return "{0}: Lade Meisteschaften von champion.gg herrunter."; } }
        public string UpdatingMasteries { get { return "{0}: Aktualisiere Meisterschaften."; } }
        public string Disconnected { get { return "Verbindung getrennt."; } }
        public string BoughtXpBoost3Days { get { return "XP Boost für 3 Tage wurde gekauft!"; } }
    }
}